package com.company;

public class Team {
   
    private Integer number;
    private String name;
    private String city;

    public Integer getNumber(){
        return number;
    }
    public void setNumber (Integer number){
        this.number = number;
    }
    public String getName(){
        return name;
    }
    public void setName (String name){
        this.name = name;
    }
    public String getCity(){
        return city;
    }
    public void setCity(String city){
        this.city = city;
    }
    public String toString(){
        return "Number: " + number + " Team Name: " + name + " City: " + city;
    }

    public Team() {
        this(0,"" ,"" );
    }

    public Team(Integer Number, String Name, String City){
        this.number = Number;
        this.name = Name;
        this.city = City;
    }
}
