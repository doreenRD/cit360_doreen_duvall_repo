package com.company;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.sql.SQLOutput;

public class Main {

    public String lessThan;
    public static String str1 = "Doreen";
    public static String str2 = "Doreen1";
    public static String str3 = str1;

    public static void main(String[] args) {

        /*// Scanner input= new Scanner (System.in);
        //System.out.println("Enter an integer:..");
        //int v1 = input.nextInt();
        //System.out.println("Enter an double value:..");
        //double v2 = input.nextDouble();*/

        System.out.println("Calculate Tax");
        double amount = 45.00;
        double rate = 0.09;
        double tax = CalculateTax(amount,rate);
        System.out.println("Tax on " + amount + " is: " + tax);

        System.out.println("Checking lessThan");
        int i = 1;
        int j = 2;
        boolean lt = lessThan(i, j);
        System.out.println(i + " is less than " + j +":  "+ lt);

        System.out.println("Set up Fibonacci series");
        Integer[] fibSeries = fibonacciSeries(9);
        for (i = 0; i < fibSeries.length; i++) {
            System.out.println("This is count " + i + ": " + fibSeries [i]);

        }

    }

    public static double CalculateTax(double purchaseAmount, double taxRate) {
        double newTax = purchaseAmount * taxRate;//I wanted to show this way and the next way of returning
        return newTax;
    }

    public static Boolean lessThan(int i, int j) {
         return (i < j);
    }
    public static Main makeMain(boolean make){
        if (make){
            return new Main();
        } else{
            return null;
        }
    }

    public static Integer[] fibonacciSeries(Integer count){
        Integer[] fibSeries = new Integer[count];
        for (int i = 0; i < fibSeries.length; i++){// warning that the condition is always false seems to be incorrect as the method works.
            fibSeries[i] = fib(i);
        }

        return fibSeries;
    }
    //Adapted from Introduction to Java Programming 10th edition page 710-711
    public static Integer fib(Integer index){
        if (index == 0) {
            return 0;
        }
        else if (index ==1) {
            return 1;
        }
        else {
            return fib(index - 1)+fib(index - 2);
        }
    }

}