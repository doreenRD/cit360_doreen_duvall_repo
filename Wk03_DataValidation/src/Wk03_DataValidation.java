//  Author:  Doreen Duvall
//  Date:   Jan 21, 2021
//  CIT360-02 - Week 3
//  Professor: Brother Tuckett


import java.util.*;
import java.util.Scanner;

public class Wk03_DataValidation {

        /*   Instructions
        Use the following requirements when writing your program:
                1.  Create a program that gathers input of two numbers from the keyboard in its main method
                        and then calls the method below to calculate.
                2.  Write a method that divides the two numbers together (num1/num2) and returns the result.
                        The method should include a throws clause for the most specific exception possible
                        if the user enters zero for num2.
                3.  In your main method, use exception handling to catch the most specific exception possible.
                        Display a descriptive message in the catch to tell the user what the problem is.
                        Give the user a chance to retry the data entry. Display a message in the final statement.
                4.  Run and show the output when there is no exception AND when a zero in the denominator is used.
5.  Rewrite the data collection so that it uses data validation to ensure an error does not occur.
        If the user does type a zero in the denominator, display a message and give the user a chance
        to retry.
6.  Run and show the output when there is no problem with data entry and when a zero is used in
        the denominator. */

    public static void main(String[] args)

    {
        while (true) {  //start loop
            System.out.println("Please enter two numbers. The first one will be divided by the second one.");
            Scanner input = new Scanner(System.in);
            System.out.println("Please input the first number: ");
            var num1 = input.nextDouble();
            System.out.println("Please enter the second number: ");
            var num2 = input.nextDouble();
            double num3;

            if (num2 == 0.0) {
                System.out.println("Should not try to divide by zero. Please start over.");
                continue; //start back at the beginning of the loop as the num2 failed validation
            }
            num3 = num1/num2;

            System.out.println("The result of " + num1 + " divided by " + num2 + " equals " + num3 + " .");
            break; //exit loop
        }
    }


}
