package com.example.Final.dbclasses;

import javax.persistence.*;

/** The Teams class corresponds with teams table
 *  in the firstrobotics database. */
@Entity
@Table(name = "teams")
public class Teams {

    /** id is an identity type field in the database and the primary key */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idTeams")
    private int idTeams;

    /** teamnumber is a number that each team has already had assigned */
    @Column(name = "teamnumber")
    private int  teamNumber;

    @Column(name = "teamname")
    private String teamName;

    /**score is supposed to start out as null, after the robot runs it is added. */
    @Column(name = "score")
    private int score;

    /** notes is for the info about how a team might work with 3805*/
    @Column(name = "notes")
    private String notes;

    /** idStudent is the student id that input the information*/
    @Column(name = "idStudents")
    private int idStudents;

    public Teams(){}

    public Teams (Integer teamnumber,String teamname, Integer score, String notes, Integer idStudents){
        this.teamNumber= teamnumber;
        this.teamName= teamname;
        this.score = score;
        this.notes = notes;
        this.idStudents = idStudents;
    }

    /** This is where the getters and setters begin.*/
    public int getIdTeams() {
        return idTeams;
    }

    public void setIdTeams(int idTeams) {
        this.idTeams = idTeams;
    }

    public int getTeamNumber() {
        return teamNumber;
    }

    public void setTeamNumber(int teamNumber) {
        this.teamNumber = teamNumber;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public int getIdStudents() {
        return idStudents;
    }

    public void setIdStudents(int idStudents) {
        this.idStudents = idStudents;
    }
}