package com.duvall;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import java.util.*;

public class TeamDBAccess {

    SessionFactory factory = null;
    Session session = null;

    private static TeamDBAccess single_instance = null;

    private TeamDBAccess()
    {
        factory = HibernateUtils.getSessionFactory();
    }

    /** Create and return instance(connection) */
    public static TeamDBAccess getInstance()
    {
        if (single_instance == null) {
            single_instance = new TeamDBAccess();
        }

        return single_instance;
    }
    // Each get method opens a session in the database.
    // This session returns a list of all the teams.
    public List<Teams> getTeams() {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from com.duvall.Teams";
            List<Teams> teamlist = (List<Teams>)session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return teamlist;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback if an error occurs.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
  }

    // This session returns one team with a specific id number.
    public Teams getTeam(int id) {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from com.duvall.Teams where id=" + Integer.toString(id);
            Teams team = (Teams)session.createQuery(sql).getSingleResult();
            session.getTransaction().commit();
            return team;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback if an error occurs.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }

    // This session returns one team with a specific teamNumber.
    public Teams getTeamByTeamNumber(int teamNumber) {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from com.duvall.Teams where teamnumber=" + Integer.toString(teamNumber);
            Teams team = (Teams)session.createQuery(sql).getSingleResult();
            session.getTransaction().commit();
            return team;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback if an error occurs.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }

    // This session creates a new team with their teamNumber and teamName.
    public Teams createTeam(Integer teamNumber,String teamName){

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            Teams team = new Teams();
            team.setTeamNumber(teamNumber);
            team.setTeamName(teamName);

            session.save(team);

            session.getTransaction().commit();
            return team;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback if an error occurs.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }
    // This session updates a team in the database with their score received at a match.
    public Teams setScoreForTeam(Integer teamNumber,Integer score){
        try {
            Teams team = getTeamByTeamNumber(teamNumber); //found that if open session is first it creates an error.
            session = factory.openSession();
            session.getTransaction().begin();
            team.setScore(score);
            session.update(team);

            session.getTransaction().commit();
            return team;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback if an error occurs.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }

}