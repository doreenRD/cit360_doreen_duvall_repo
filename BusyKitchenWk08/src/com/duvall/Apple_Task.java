package com.duvall;

public class Apple_Task implements Runnable {

    private int numApples;
    private int peelDelay = 500;
    private int eatDelay=1000;


    public Apple_Task(int count){
        this.numApples = count;
    }

    public void run(){
        System.out.println("\n\n    Starting to peel apples");
        for (int peel = 1; peel <= numApples; peel++){
            try{
                Thread.sleep(peelDelay);
            } catch (InterruptedException e) {
                System.err.println(e.toString() + "I can't peel apple # : " + peel + ".");
            }
            System.out.println( "    Peeled Apple # :" + peel+ ".");
        }
        System.out.println("\n\n    Starting to eat my delicious apples");
        for (int eat = 1; eat <= numApples; eat++){
            try{
                Thread.sleep(eatDelay);
            } catch (InterruptedException e) {
                System.err.println(e.toString() + "I can't finish apple # : " + eat + ".");
            }
            System.out.println( "    Eating Apple # :" + eat+ ".");
        }
        System.out.println("\n    Apples are gone! =(\n");
    }
}
