package com.example.Final.api;

import com.example.Final.dbclasses.Student;
import com.example.Final.dbclasses.dbconnector;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "CreateStudentServlet", urlPatterns = "/CreateStudentServlet")
public class CreateStudentServlet extends HttpServlet {
    private String message;

    public void init(){message = "Nothing to get";}

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        // Set Content Response Type
        PrintWriter out = response.getWriter();
        //response.setContentType("application/json");
        //HttpSession session = request.getSession();
        response.setContentType ("text/html");
        out.println("<html><head></head><body>");
        String firstname = request.getParameter("firstName");
        String lastname = request.getParameter("lastName");
        String username = request.getParameter("userName");
        String password = request.getParameter("password");
        out.println("<h1> Student has created their account</h1>");
        out.println("<h2><p> First Name: " + firstname + "</p></h2>");
        out.println("<h2><p> Last Name: " + lastname + "</p></h2>");
        out.println("<h2><p> Username: " + username + "</p></h2>");
        out.println("<h2><p> Password: " + "****" + "</p></h2>");
        out.println("</body></html>");

        dbconnector db =  dbconnector.getInstance();
        Student newstudent = db.createStudent(firstname, lastname, username, password);
        out.print(true);
    }

    public void destroy() {
    }
}