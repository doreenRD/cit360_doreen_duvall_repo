package com.duvall;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {

    public static void main(String[] args) {
        ExecutorService myService = Executors.newFixedThreadPool(3);

        Apple_Task apple  = new Apple_Task(7);
        Slaw_Task slaw = new Slaw_Task(10);
        MnM_Task MnM  = new MnM_Task(3);

        myService.execute(apple);
        myService.execute(slaw);
        myService.execute(MnM);

        myService.shutdown();

    }
}
