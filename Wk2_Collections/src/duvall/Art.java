package duvall;

public class Art {

        private String name;
        private String artist;

        public Art(String name, String artist){
            this.name = name;
            this.artist = artist;
        }
        public String toString(){
            return "Painting Name: " + name + "  Artist: " + artist;
        }
}
