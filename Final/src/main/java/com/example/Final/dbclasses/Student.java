package com.example.Final.dbclasses;
import javax.persistence.*;
@Entity
@Table(name = "students")
public class Student {
    /** id is an identity type field in the database and the primary key */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idStudents")
    private int idStudents;

    /** firstname is a string that each student is required to input */
    @Column(name = "firstname")
    private String firstName;

    /** lastname is a string that each student is required to input */
    @Column(name = "lastname")
    private String lastName;

    /**username is a string the student chooses for their username and must input. */
    @Column(name = "username")
    private String userName;

    /**password is a string the student chooses for their password and must input. */
    @Column(name = "password")
    private String password;

    public Student(){};

    public Student(String firstname,String lastname, String username, String password){
        this.firstName= firstname;
        this.lastName = lastname;
        this.userName = username;
        this.password = password;
    }
    public int getIdStudents() {
        return idStudents;
    }
    public void setIdStudents(int id) {
        this.idStudents = id;
    }

    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstname) {
        this.firstName = firstname;
    }

    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastname) {
        this.lastName = lastname;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String username) {
        this.userName = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
