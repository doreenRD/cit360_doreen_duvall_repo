package com.duvall;

import javax.persistence.*;

/** The Teams class corresponds with teams table
 *  in the firstrobotics database. */
@Entity
@Table(name = "teams")
public class Teams {

    /** id is an identity type field in the database and the primary key */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    /** teamnumber is a number that each team has already had assigned */
    @Column(name = "teamnumber")
    private Integer teamNumber;

    @Column(name = "teamname")
    private String teamName;

    /**score is supposed to start out as null, after the robot runs it is added. */
    @Column(name = "score")
    private Integer score;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getTeamNumber() {
        return teamNumber;
    }

    public void setTeamNumber(Integer teamNumber) {
        this.teamNumber = teamNumber;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public String toString() {
        return Integer.toString(id) + ": " + teamNumber + ": " + teamName + ", " + score;
    }
}