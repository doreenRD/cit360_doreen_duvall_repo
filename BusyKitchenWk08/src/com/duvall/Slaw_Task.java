package com.duvall;

public class Slaw_Task implements Runnable{

    private int numServings;
    private int sliceDelay = 750;
    private int sauceDelay = 100;


    public Slaw_Task(int count){
        this.numServings = count;
    }

    public void run(){
        System.out.println("\n\n  Starting to slice cabbage");
        for (int slice = 1; slice <= numServings; slice++){
            try{
                Thread.sleep(sliceDelay);
            } catch (InterruptedException e) {
                System.err.println(e.toString() + "I can't slice cabbage # : " + slice + ".");
            }
            System.out.println( "  Sliced Cabbage serving # :" + slice+ ".");
        }
        System.out.println("\n\n  Mixing sauce into cabbage");
        for (int sauce = 1; sauce <= numServings; sauce++){
            try{
                Thread.sleep(sauceDelay);
            } catch (InterruptedException e) {
                System.err.println(e.toString() + "I can't add sauce to serving # : " + sauce + ".");
            }
            System.out.println( "  Mixing slaw serving # :" + sauce+ ".");
        }
        System.out.println("\n  Slaw is ready now! =)\n");
    }
}
