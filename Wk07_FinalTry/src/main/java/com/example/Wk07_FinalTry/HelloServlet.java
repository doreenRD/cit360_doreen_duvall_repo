package com.example.Wk07_FinalTry;

import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "HelloServlet", urlPatterns = "/HelloServlet")
public class HelloServlet extends HttpServlet {
    private String message;

    public void init() {
        message = "Nothing to get";
    }


    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        PrintWriter out = response.getWriter();
        response.setContentType ("text/html");
        out.println("<html><head></head><body>");
        String teamNumber = request.getParameter("teamNumber");
        String teamName = request.getParameter("teamName");
        String score = request.getParameter("score");
        out.println("<h1> Team Information by Match</h1>");
        out.println("<h2><p> Team Number: " + teamNumber + "</p></h2>");
        out.println("<h2><p> Team Name: " + teamName + "</p></h2>");
        out.println("<h2><p> Score: " + score + "</p></h2>");
        out.println("</body></html>");

    }


    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");

        // Hello
        PrintWriter out = response.getWriter();
        out.println("<html><body>");
        out.println("<h1>" + message + "</h1>");
        out.println("</body></html>");
    }

    public void destroy() {
    }
}