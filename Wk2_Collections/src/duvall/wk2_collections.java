package duvall;

import java.util.*;

public class wk2_collections {
        public static void main(String[] args) {

            System.out.println("\n_List- collection items are ordered as input. Duplicates are allowed.\n");
            ArrayList<String> wordsList = new ArrayList<>();
            wordsList.add("friend");
            wordsList.add("Doreen");
            wordsList.add("James");
            wordsList.add("Caroline");
            wordsList.add("yours");
            wordsList.add("mine");
            wordsList.add("Hello");
            wordsList.add("Adam");
            wordsList.add("friend");
            System.out.println(wordsList);


            System.out.println("\n_Set- collection set are unordered. Alphabetized on output. Duplicates are not allowed.");
            Set<String> wordset = new TreeSet<>();
            wordset.add("friend");
            wordset.add("Doreen");
            wordset.add("James");
            wordset.add("Caroline");
            wordset.add("yours");
            wordset.add("mine");
            wordset.add("Adam");
            wordset.add("friend"); /*This is a duplicate and will not be added to the set*/

            for (Object str : wordset) {
                System.out.println((String) str);
            }




            System.out.println("\n_Queue - collection queue(can be used as FIFO, LILO, Or priority ordering. No null elements" +
                    "Can use add(), offer(), remove(), poll(), element(), peek().");

            Queue<String> queue = new PriorityQueue<>();
            queue.add("friend");
            queue.add("Doreen");
            queue.add("James");
            queue.add("Caroline");
            queue.add("yours");
            queue.add("mine");
            queue.add("Hello");
            queue.add("Adam");
            queue.add("friend");

            /* for didn't work for queue - Bro Tuckett showed an iterator. This prints out Capitals in alpha followed by others in alpha. */

            Iterator<String> iterator = queue.iterator();
            while (iterator.hasNext()) {
                System.out.println(queue.poll());
            }



            System.out.println("\n_Map - needs pairs #reference/value. If order is important use HashMap,Insert order or sort by key use TreeMap or linkedHashMap, ");

            HashMap<Integer,String> mapWord = new HashMap<>();
            mapWord.put(1,"friend");
            mapWord.put(2,"Doreen");
            mapWord.put(3,"James");
            mapWord.put(4,"Caroline");
            mapWord.put(5,"yours");
            mapWord.put(6,"mine");
            mapWord.put(7,"Hello");
            mapWord.put(8,"Adam");
            mapWord.put(5,"friend"); /* Replaces the previous item '5'*/

            for(int i=1; i<10;i++) {
                String result = mapWord.get(i);
                System.out.println(result);
            }

            System.out.println("\n---My List using Generics---");
            List<Art> myList = new LinkedList<>();
            myList.add(new Art("Mona Lisa", "Leonardo da Vinci"));
            myList.add(new Art("The Starry Night", "Vincent van Gogh"));
            myList.add(new Art("Impression, Sunrise", "Claude Monet"));
            myList.add(new Art("Girl with a Pearl Earring", "Johannes Vermeer"));
            myList.add(new Art("Guernica", "Pablo Picasso"));

            for (Art art : myList){
                System.out.println(art);
            }






        }
}
