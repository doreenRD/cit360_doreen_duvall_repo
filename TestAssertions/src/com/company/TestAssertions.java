package com.company;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.*;

public class TestAssertions {

             public double CalculateTax(double purchaseAmount, double taxRate) {
                 double computeTax = purchaseAmount * taxRate;
                 return computeTax;
             }

             @Test
             @DisplayName("AssertEquals tax of 9% on 45.00")
             void testTax() {
                assertEquals(4.05, CalculateTax(45, .09));
             }

     }





  /*  @Test
    public void testAssertions(){
        //test data
        String str1 = new String ("abc");
        String str2 = new String ("abc");
        String str3 = new String (null);
        String str4 = "abc";
        String str5 = "abc";

        int val1 = 5;
        int val2 = 6;

        String[] expecctedArray = {"one", "two", "three"};
        String[] resultArray = {"one", "two", "three"};

        //Check that two objects are equal
        assertEquals(str1, str2);
    }*/




