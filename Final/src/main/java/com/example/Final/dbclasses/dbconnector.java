package com.example.Final.dbclasses;

import com.example.Final.api.LoginServlet;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.List;

public class dbconnector {

    SessionFactory factory = null;
    Session session = null;

    private static dbconnector single_instance = null;

    private dbconnector()
    {
        factory = HibernateUtils.getSessionFactory();
    }

    /** Create and return instance(connection) */
    public static dbconnector getInstance()
    {
        if (single_instance == null) {
            single_instance = new dbconnector();
        }

        return single_instance;
    }
    // This session checks a Students login, username and password.
    public Student checkLogin(String username, String password){


        try {
            session = factory.openSession();
            session.getTransaction().begin();
            System.out.println(username);
            System.out.println(password);
            String hql = "from com.example.Final.dbclasses.Student where userName= :username and password= :password";
            Student student = (Student) session.createQuery(hql)
                    .setParameter("username",username)
                    .setParameter("password",password).getSingleResult();
            session.getTransaction().commit();
            return student;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback if an error occurs.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }
    // This session creates a new Student with their first name, last name, username and password.
    public Student createStudent(String firstname,String lastname, String username, String password){


        try {
            session = factory.openSession();
            session.getTransaction().begin();
            Student student = new Student();
            student.setFirstName(firstname);
            student.setLastName(lastname);
            student.setUserName(username);
            student.setPassword(password);


            session.save(student);

            session.getTransaction().commit();
            return student;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback if an error occurs.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }
    // Each get method opens a session in the database.
    // This session returns a list of all the teams.
    public List<Teams> getTeams() {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from com.example.Final.dbclasses.Teams";
            List<Teams> teamlist = (List<Teams>)session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return teamlist;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback if an error occurs.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
  }

    // This session returns one team with a specific id number.
    public Teams getTeam(int idTeams) {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from com.example.Final.dbclasses.Teams where idTeams=" + Integer.toString(idTeams);
            Teams team = (Teams)session.createQuery(sql).getSingleResult();
            session.getTransaction().commit();
            return team;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback if an error occurs.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }

    // This session returns all infor of a team by a specific teamNumber.
    public List<Teams> getAllTeamInfoByTeamNumber(int teamNumber) {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from com.example.Final.dbclasses.Teams where teamNumber=" + Integer.toString(teamNumber);
            List<Teams> teams = (List<Teams>) session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return teams;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback if an error occurs.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }

    // This session returns one team with a specific teamNumber.
    public Teams getTeamByTeamNumber(int teamNumber) {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from com.example.Final.dbclasses.Teams where teamNumber=" + Integer.toString(teamNumber);
            Teams team = (Teams)session.createQuery(sql).getSingleResult();
            session.getTransaction().commit();
            return team;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback if an error occurs.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }

    // This session creates a new team with their teamNumber and teamName.
    public Teams createTeam(Integer teamNumber,String teamName,Integer studentId){

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            Teams team = new Teams();
            team.setTeamNumber(teamNumber);
            team.setTeamName(teamName);
            team.setIdStudents(studentId);

            session.save(team);

            session.getTransaction().commit();
            return team;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback if an error occurs.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }
    // This session updates a team in the database with their score received at a match and any notes.
    public Teams setScoreNotesForTeam(Integer teamNumber,Integer score,String notes){
        try {
            Teams team = getTeamByTeamNumber(teamNumber); //found that if open session is first it creates an error.
            session = factory.openSession();
            session.getTransaction().begin();
            team.setScore(score);
            team.setNotes(notes);
            session.update(team);

            session.getTransaction().commit();
            return team;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback if an error occurs.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }
    // This session creates a team in the database with all information available
    public Teams setAllForTeam(Integer teamNumber,String teamName, Integer score,String notes,Integer studentId){
        try {
            session = factory.openSession();
            session.getTransaction().begin();
            Teams team = new Teams();
            team.setTeamName(teamName);
            team.setTeamNumber(teamNumber);
            team.setScore(score);
            team.setNotes(notes);
            team.setIdStudents(studentId);
            session.save(team);
            session.getTransaction().commit();
            return team;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback if an error occurs.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }

}