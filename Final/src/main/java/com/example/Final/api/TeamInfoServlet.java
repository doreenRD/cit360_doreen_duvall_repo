package com.example.Final.api;

import com.example.Final.dbclasses.Student;
import com.example.Final.dbclasses.Teams;
import com.example.Final.dbclasses.dbconnector;

import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import javax.websocket.Session;

@WebServlet(name = "apiTeamInfoServlet", urlPatterns = "/api/TeamInfoServlet")
public class TeamInfoServlet extends HttpServlet {
    private String message;

    public void init() {
        message = "Nothing to get";
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        PrintWriter out = response.getWriter();
        response.setContentType ("text/html");
        HttpSession session = request.getSession();
        Student student = (Student) session.getAttribute("student");
        out.println("<html><head></head><body>");

        if(session.getAttribute("student") != null) {
            String strTeamNumber = request.getParameter("teamNumber");

            int teamNumber = Integer.parseInt(strTeamNumber);
            String teamName = request.getParameter("teamName");

            String strScore = request.getParameter("score");

            int score = Integer.parseInt(strScore);
            String notes = request.getParameter("notes");
            out.println("<h1> Team Information by Match</h1>");
            out.println("<h2><p> Team Number: " + teamNumber + "</p></h2>");
            out.println("<h2><p> Team Name: " + teamName + "</p></h2>");
            out.println("<h2><p> Score: " + score + "</p></h2>");
            out.println("<h2><p> Notes: " + notes + "</p></h2>");

            dbconnector db = dbconnector.getInstance();
            Teams createTeam = db.setAllForTeam(teamNumber, teamName, score, notes, student.getIdStudents());
            if (createTeam != null) {
                out.println("<h2>Team and Information added to database successfully</h2>");
            } else {
                out.println("<h2>Team failed to be added to database.</h2>");
            }
            out.print(true);
        } else {
            out.print("You must be logged in to view this.");
        }
        out.println("</body></html>");
    }


    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");

        // Hello
        // Set Content Response Type
        response.setContentType("application/json");
        // Creates Response Writer
        PrintWriter out = response.getWriter();
        // Gets User Session
        HttpSession session = request.getSession();
        String name= (String) session.getAttribute("name");
        out.println("<html><body>");
        out.println("<h1>Hello " + name + "</h1>");
        out.println("</body></html>");
    }

    public void destroy() {
    }
}