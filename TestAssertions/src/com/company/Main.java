package com.company;

public class Main {

    public static void main(String[] args) {
	// write your code here
         // Scanner input= new Scanner (System.in);

        System.out.println("Calculate Tax");
        double purchaseAmount = 45.00;
        double taxRate = 0.09;

        TestAssertions t = new TestAssertions();

        double newTax = t.CalculateTax(purchaseAmount, taxRate );
        System.out.println("Tax on " + purchaseAmount + " is: " + newTax);
    }
}
