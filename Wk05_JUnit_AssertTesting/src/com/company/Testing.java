package com.company;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class Testing {
    Main m = new Main();

    @Test
    @DisplayName("AssertEquals tax of 9% on 45.00")
    void testTax() {
        assertEquals(4.05, Main.CalculateTax(45, .09));
    }
    @Test
    @DisplayName("AssertTrue/false on lessThan method")
    void testLessThan() {
        assertTrue(Main.lessThan(1,2));
        assertFalse(Main.lessThan(18,2));
    }
    @Test
    @DisplayName("This test should fail on lessThan method")
    void failTestTrue() {
        assertTrue(Main.lessThan(2,1), "This has failed");
    }
    @Test
    @DisplayName("This test should fail on lessThan method")
    void failTestTrue2() {
        assertFalse(Main.lessThan(1,2), "This is the second failure");
    }
    @Test
    @DisplayName("AssertNull and NotNull to find out if there is something in mainNull")
    void testNull() {
        Main mainNull = null;
        assertNull(mainNull);
        assertNotNull(m); // My main object in this file is not null.
        // This is checking my makeMain method in the main.java file.
        assertNull(Main.makeMain(false));
        assertNotNull(Main.makeMain(true)); //I expect this to return a non-Null object
    }
    @Test
    @DisplayName("Testing to see if strings point to the same object")
    void testSame() {
        assertSame(Main.str1, Main.str3);
        assertNotSame(Main.str1, Main.str2);
    }

    @Test
    @DisplayName("Testing Array members")
    void testArray(){
        Integer[] expected = {0, 1, 1, 2, 3, 5, 8, 13, 21};
        assertArrayEquals(expected, Main.fibonacciSeries(9));
    }




}
