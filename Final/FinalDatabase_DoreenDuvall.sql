-- MySQL dump 10.13  Distrib 8.0.23, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: firstrobotics
-- ------------------------------------------------------
-- Server version	8.0.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `students`
--

DROP TABLE IF EXISTS `students`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `students` (
  `idStudents` int NOT NULL AUTO_INCREMENT,
  `firstname` varchar(45) NOT NULL,
  `lastname` varchar(45) NOT NULL,
  `username` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  PRIMARY KEY (`idStudents`),
  UNIQUE KEY `username_UNIQUE` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `students`
--

LOCK TABLES `students` WRITE;
/*!40000 ALTER TABLE `students` DISABLE KEYS */;
INSERT INTO `students` VALUES (5,'doreen','duvall','doreen','password'),(9,'Doreen','Duvall','dd','pw'),(10,'Jim','Duvall','jd','pw'),(11,'Renee','Peterson','rp','pw'),(12,'Amy','Duvall','ad','pw'),(13,'Suzy','McKnight','Suzy','pw'),(14,'Camille','Duvall','cd','pw'),(15,'johnny','depp','jon','pw'),(16,'Cathy','Webb','cw','pw');
/*!40000 ALTER TABLE `students` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teams`
--

DROP TABLE IF EXISTS `teams`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `teams` (
  `idTeams` int NOT NULL AUTO_INCREMENT,
  `teamnumber` int NOT NULL,
  `teamname` varchar(60) NOT NULL,
  `score` int DEFAULT NULL,
  `notes` varchar(255) DEFAULT NULL,
  `idStudents` int DEFAULT NULL,
  PRIMARY KEY (`idTeams`),
  KEY `idStudents_idx` (`idStudents`),
  CONSTRAINT `idStudents` FOREIGN KEY (`idStudents`) REFERENCES `students` (`idStudents`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
--
--
-- Dumping data for table `teams`
--

LOCK TABLES `teams` WRITE;
/*!40000 ALTER TABLE `teams` DISABLE KEYS */;
INSERT INTO `teams` VALUES (11,222,'TheDs',0,NULL,9),(12,890,'Bert',0,NULL,9),(13,890,'Bert',0,NULL,9),(14,333,'sugary Robotics',0,NULL,9),(15,3805,'Atomic Robotics',0,NULL,9),(16,2541,'Tiddlywinks',55,'We are testing this now.',5),(17,3491,'HotTopicks',90,'',5),(18,2504,'CanDos',60,'Testing now',9),(19,6502,'CanDon\'ts',72,'We are testing this now.',12),(20,890,'Bert',45,'Testing now',12),(21,3805,'Atomic Robotics',46,'Testing again. ',13),(22,3805,'Atomic Robotics',46,'Wow - again really?',13),(23,3805,'Atomic Robotics',46,'We are testing this now.',9),(24,3805,'Atomic Robotics',46,'Wow - again please!',9),(25,2450,'CannonBalls',46,'Testing again2.',15),(26,3805,'Atomic Robotics',74,'another test!',9);
/*!40000 ALTER TABLE `teams` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-04-07 20:06:17
