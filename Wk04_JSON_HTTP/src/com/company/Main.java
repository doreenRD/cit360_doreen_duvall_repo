package com.company;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import java.util.ArrayList;
import java.util.List;


public class Main {

    /*
    Write an application with two separate components.
        The first is an HTTP server component that when called takes an object
            and converts it to JSON using the Jackson JSON parser, and returns a block
            of JSON using HTTP.
        The second component is a client component.  It makes a call to the server component,
            gets the JSON it returns, and then converts that using the Jackson JSON parser, into an object.
    These two components need to be written as two separate classes.
    Make sure you incorporate error handling and data validation into your programs, including catching
        the most specific type of exceptions you can.
   */

    public static void main(String[] args) {
        try{
            DoreensHttpServer.startServer();//calling server to start
        }catch(Exception e){
            System.out.println("Unable to start server." + e.toString());
        }
        //getting the list of teams from the server in JSON
        String teamsJSON = getJSONContent("http://localhost:9999/getTeams");
        System.out.println("\nSuccessful request: " +teamsJSON);
        //converting the JSON list of teams into Team objects.
        List<Team> teamList = convertJSONtoTeams(teamsJSON);
        System.out.println("Successful conversion to objects\n");
        for (Team t: teamList){ //for each team on the list print out
            System.out.println(t);
        }
        DoreensHttpServer.stopServer();//We're done stop the server
    }





    public static String getJSONContent(String requestString){
        String content = "";

        try {
            //make a http connection to the requested server
            URL serverURL = new URL(requestString);
            HttpURLConnection conn2ds = (HttpURLConnection) serverURL.openConnection();
            //setting up the reader to get the data from doreens http server.
            BufferedReader myreader = new BufferedReader(new InputStreamReader(conn2ds.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();
            String line;//doesnt need = null;
            //read lines of data until all are received
            while ((line = myreader.readLine()) != null){
                stringBuilder.append(line);
                stringBuilder.append("\n");
            }
            //put them into the return string
            content = stringBuilder.toString();
            //catching potential exceptions
        } catch (MalformedURLException mue){
            System.out.println("Malformed URL: " + mue.toString());
        } catch (IOException ioe){
            System.out.println("Error opening the connection: "+ ioe.toString());
        }
        return content;
    }

    //coverting my JSONstring to a list of Team objects
    public static List<Team> convertJSONtoTeams(String strTeamList){
        ObjectMapper objectMapper = new ObjectMapper();
        try{
            /*
            Dont need but makes sense!
            List<Team> teamList = objectMapper.readValue(s, new TypeReference<List<Team>>(){});
            return teamList;
            */
            return objectMapper.readValue(strTeamList, new TypeReference<List<Team>>(){});
        }catch (JsonProcessingException jpe) {
            System.out.println("Unable to convert JSON to Object. " + jpe.toString());
        }
        //If failed return an empty list.
        return new ArrayList<Team>();
    }
}
