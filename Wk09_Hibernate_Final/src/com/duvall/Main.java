//This is doreen duvall's code for CIT360 Winter 2021 (based off of Bro. Tucketts code)

package com.duvall;

import java.util.*;

public class Main {

    public static void main(String[] args) {

        TeamDBAccess ta = TeamDBAccess.getInstance();

        //Selects all of the teams in the database
        List<Teams> teamlist = ta.getTeams();
        for (Teams team : teamlist) {
            System.out.println(team);

        }

        //Selects only the team with id #1 from the database and prints it.
        System.out.println(ta.getTeam(1));

        // Creates Team (ie. inserts)
//        Teams newTeam = ta.createTeam(1234,"Sierra Sharks");
//        Teams newTeam = ta.createTeam(8628,"Chickenados");
//        System.out.println(ta.getTeam(6));

        // Updates score by team number
 //       Teams newTeam = ta.setScoreForTeam(1234,10);
        Teams newTeam = ta.setScoreForTeam(8628, 32);
        System.out.println(ta.getTeam(6));
    }
}
