package com.example.Final.api;

import com.example.Final.dbclasses.Student;
import com.example.Final.dbclasses.dbconnector;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "LoginServlet", urlPatterns = "/api/LoginServlet")
public class LoginServlet extends HttpServlet {

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        PrintWriter out = response.getWriter();
        //this sets the response type of the Login information
        response.setContentType("text/html");

        String username = request.getParameter("username");
        String password = request.getParameter("password");

        // Creates an instance of the DB Connector
        dbconnector db = dbconnector.getInstance();
        Student student = db.checkLogin(username,password);

        if (student != null){
            session.setAttribute("authenticated",1);
            session.setAttribute("student",student);

            // How to get the id/student object from session after login
            out.print(username + " is signed in.");
            //Sends me to the main page to choose what I want to do next.
            response.sendRedirect(request.getContextPath() + "/index.jsp");

            //
        } else {
            out.print("Please create your username and password");
            //call the CreateStudent.html file
        }
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        // Set Content Response Type
        response.setContentType("application/json");
        // Creates Response Writer
        PrintWriter out = response.getWriter();
        // Gets User Session
        HttpSession session = request.getSession();
        String username = request.getParameter("username");
        String password = request.getParameter("password");

        // Creates instance of the DB Connector
        dbconnector db = dbconnector.getInstance();



        if (username.equals(username) && password.equals("password")){

            session.setAttribute("authenticated",1);
            //session.setAttribute("student",studentObject);
            out.print(username + " is signed in.");
            //
        } else {
            out.print("Please create your username and password");
            //call the CreateStudent.html file
        }
    }
    public void destroy() {
    }
}