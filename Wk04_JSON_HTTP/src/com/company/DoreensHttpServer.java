package com.company;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;

public class DoreensHttpServer {
    static HttpServer dserver;
    //start the http server dserver.
    public static void startServer() throws Exception{
        //create a server and assign a port number. (use default size for backlog)
        dserver = HttpServer.create(new InetSocketAddress(9999),0);
        //tell the server which handler to use when it gets this request.
        dserver.createContext("/getTeams", new getTeamsHandler());
        dserver.start();
    }

    public static void stopServer(){
        dserver.stop(0);//stop the server, the number is how long to wait.
    }


    //convert the object Team to JSON
    public static String convertTeamToJSON(Team[] teamlist){
        ObjectMapper teamListMapper = new ObjectMapper();
        String strTeamList = "";
        //
        try {
            strTeamList = teamListMapper.writeValueAsString(teamlist);
        }
        catch(JsonProcessingException ex) {
            System.out.println("Unable to convert Team list to JSON: " + ex.toString());
        }
        return strTeamList;
    }



    //set up my array to be used
    public static Team[] getTeams(){
        Team[] teamlist = {//FTC Washington State Teams
            new Team(3805, "Atomic Robotics", "Edmonds"),
            new Team(11104, "Bearded Pineapples", "Kirkland"),
            new Team(6541, "Titan Robotics II", "Bellevue"),
            new Team(7760, "Mishap", "Duvall")
        };
        return teamlist;
     }



     //create the http handler class which must have a handler method
     private static class getTeamsHandler implements HttpHandler {
        public void handle(HttpExchange exchange) throws IOException {
            //Convert the team list to JSON
            Team[] teamlist = getTeams();
            String jsonTeamlist = convertTeamToJSON(teamlist);
            //Send the team list to the client using the exchange object.
            //First send header a code and # of bytes in the response.
            exchange.sendResponseHeaders(200, jsonTeamlist.length());
            //next send the team list data to the exchanges output stream.
            OutputStream osTeam = exchange.getResponseBody();
            osTeam.write(jsonTeamlist.getBytes());
            //Close the output stream to complete the exchange.
            osTeam.close();
        }
     }



}
