package com.example.Final.api;

import com.example.Final.dbclasses.Teams;
import com.example.Final.dbclasses.dbconnector;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;


@WebServlet(name = "getTeamInfoServlet", value = "/api/getTeamInfoServlet")
public class getTeamInfoServlet extends HttpServlet {

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();

        int teamNumber = Integer.parseInt(request.getParameter("teamNumber"));
        List<Teams> teams = dbconnector.getInstance().getAllTeamInfoByTeamNumber(teamNumber);

        out.print(convertObjectToJson(teams));
    }

        //convert the object Team to JSON
    public static String convertObjectToJson(Object obj) {
        ObjectMapper teamListMapper = new ObjectMapper();
        String strTeamList = "";
        //this makes it readable
        try {
            strTeamList = teamListMapper.writerWithDefaultPrettyPrinter().writeValueAsString(obj);
        } catch (JsonProcessingException ex) {
            System.out.println("Unable to convert obj to JSON: " + ex.toString());
        }
        return strTeamList;
    }
    public void destroy() {
    }
    }
