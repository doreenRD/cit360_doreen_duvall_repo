package com.duvall;

public class MnM_Task implements Runnable {

    private int numMnMBags;
    private int numMsPerBags = 20;
    private int openDelay = 10;
    private int eatDelay=50;


    public MnM_Task(int count){
        this.numMnMBags = count;
    }

    public void run(){
        System.out.println("\n\n      Nobody's looking - opening the M & M bags");
        for (int bag = 1; bag <= numMnMBags; bag++){
            try{
                Thread.sleep(openDelay);
            } catch (InterruptedException e) {
                System.err.println(e.toString() + "I can't open M & M bag # : " + bag + ".");
            }
            System.out.println( "      Opened bag number # :" + bag+ ".");
            for (int eat = 1; eat <= numMsPerBags; eat++) {
              try {
                  Thread.sleep(eatDelay);
              } catch (InterruptedException e) {
                  System.err.println(e.toString() + "I can't eat M & M # : " + eat + ".");
              }
              System.out.println("      Eating M & M # :" + eat + " from bag # : " + bag + ".");
          }
        }
        System.out.println("\n      Oh my M & M's sure give me a tummy ache! =/\n");
    }
}
